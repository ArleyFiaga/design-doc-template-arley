# Titulo: Template de documento de diseño
# BuildmyPC
https://gitlab.com/ucatolic
## Overview: Problema a resolver
El presente proyecto consiste en la creación de una aplicación web de artículos tecnológicos, el cual justifica la necesidad de contar con mecanismos adecuados para la búsqueda de los diferentes componentes tecnológicos en las múltiples plataformas en línea; para lograr tal fin es necesario establecer los requisitos que debe cumplir el sistema.

### Alcance(Scope)
El alcance del proyecto está definido por el diseño, desarrollo e implementación de una aplicacion web para  diversos usuarios, esta aplicacion web  permitirá a los usuario  armar su pc de acuerdo a su presupuesto y a su necesidad.
#### Casos de uso
Filtros de búsqueda para generar la mejor construcción de hardware dependiendo de la necesidad del usuario.
* Caso de uso 1
```plantuml
@startuml
User --> (Filtro de busqueda)
(Filtro de busqueda) <-- (Presupuesto)
(Filtro de busqueda) <-- (Tipo de busqueda)
(Filtro de busqueda) <-- (Diseño)
(Filtro de busqueda) <-- (Refrigeracion)
(Filtro de busqueda) <-- (Tamaño)
@enduml
```
Crear la build especifica de acuerdo a las opciones que haya elegido el usuario.
* Caso de uso 2
```plantuml

@startuml
User --> (Crear Build)
(Crear Build) --> (Listado de equipos)
@enduml
```
Se genera un grid con los elementos necesarios para la construcción completa de la torre.
* Caso de uso 3
```plantuml
@startuml
User --> (Generar Grid)
(Generar Grid) <-- (Procesador)
(Generar Grid) <-- (Disco Duro)
(Generar Grid) <-- (RAM)
(Generar Grid) <-- (MotherBoard)
(Generar Grid) <-- (Tarjeta Grafica)
(Generar Grid) <-- (Fuente)
(Generar Grid) <-- (Chasis)
@enduml
```
El usuario tendrá la posibilidad de agregar builds a favoritos y que sean almacenadas en base de datos, esto va a requerir que el usuario se haya logueado.
* Caso de uso 4
```plantuml
@startuml
User --> (Boton favorito)
(Boton favorito) <-- (Quitar en lista)
(Boton favorito) --> (Guardar en lista)
@enduml
```
* Caso de uso 5
```plantuml
@startuml
: UsuarioRegistrado: as UsuarioRegistrado 
Usuario  <|-- UsuarioRegistrado
UsuarioRegistrado <|-- admin
@enduml
```

#### Out of Scope (casos de uso No Soportados)

## Arquitectura
```plantuml
@startditaa


                                                                                                                                +--------+
                                                                                                                                |Recurso |
                                                                                                                          +---->|cBLU    |<--------+        
                                                                                                                          |  +->|        |         |        
                   |                                                                                                      |  |  +--------+         |             
             +-----+-----+                                                                                    +-----------+  |                     |    
             | cBLU      |                                                                                    |           |  |                     |
             | Servidor  |                                                                                    |           |  +->+--------+         |                        
             | seguridad |                                       +-------------+                              |           |  |  |Recuro  |         |               
             |(opcional) |                                       | Servidores  |------------------------------+-----------+--+  |cBLU    |<--------+        
             |           |                                       |     de      |                              |           |     |        |         |          
             +-----+-----+                                       | aplicaciones|---------------------------+  |           +---->+--------+         |                   
                   |                                             |  +---------+|                           |  |                                    |   
                   |                                  +----------|->| Identuty||   cluster de              |  |                                    |   
                   |                                  |          |  | Manager ||  base de datos            |  |                                    |   
+--------------+   |                                  |          |  | cRED    ||------------------+        |  |                                    |
| cBLU         |   |                                  |          |  +---------+|                  |        |  |                                    |
|  Interfaz    |   |                                  |          | cBLU        |                  |        |  |                                    |
|     de       +---+------+                           |          +-------------+                  |        |  |                                    |  
|Administrador |   |      |    +--------------+       |                                       /---+-----\  |  |    +----------------------+    +---+-------+
|              |   |      |    | cPNK         |       |                                       |cAAA     |  +--+--->|    Dispositivos      |--->|Dispositivo|    
+--------------+   |      |    | Equilibrador |       |                                       | Deposito|     |    |      de red de       |    | de red de |
                   |      +--->|      de      +-------+                                       |{s}      |     |    | conmutacion por error|    |conmutacion|
+--------------+   |      |    |    carga     |       |                                       |         |  +--+--->|    cBLU              |--->| por error |
| cBLU         |   |      |    |              |       |                                       \---+-----/  |  |    +----------------------+    |cAAA       |
|  Interfaz    |   |      |    +--------------+       |                                           |        |  |                                +-----------+    
|     de       +---+------+                           |          +-------------+                  |        |  |
|usuario final |   |                                  |          | Servidores  |                  |        |  |
|              |   |                                  |          |     de      |                  |        |  |
+--------------+   |                                  |          | aplicaciones|------------------+        |  |
                   |                                  |          |  +---------+|                           |  |
                   |                                  +----------|->| Identuty||                           |  |
                                                                 |  | Manager ||                           |  |
                                                                 |  |  cRED   |+---------------------------+  | 
                                                                 |  +---------+|                              |
                                                                 | cBLU        |------------------------------+
                                                                 +-------------+
@endditaa
```
### Diagramas
poner diagramas de secuencia, uml, etc
* Diagrama de secuencia 1 Modificar datos.
```plantuml
@startuml
skinparam style strictuml
actor User #red
actor Administrador #blue
User-> PortalSibu : 1. INGRESAR AL SISTEMA
User-> PortalSibu : 2. INICIAR SESION
User-> PortalSibu : 3. UBICAR MENU DE ADMINISTRADOR 
User-> PortalSibu : 4. VISUALIZAR FORMULARIO
User-> PortalSibu : 5. DILIGENCIAR CAPOS
PortalSibu-> BadesDeDatos : 5.1 ENVIAR DATOS
BadesDeDatos-> Administrador : 6. VERIFICAR REGISTROS
Administrador --> User : 7.ACEPTAR REGISTRO
actor Administrador
@enduml
```
*Diagrama de secuencia 2 - Iniciar sesion.
```plantuml
@startuml
skinparam style strictuml
actor User #red
User-> Sibu : 1. INGRESAR DATOS DE USUARIO
Sibu-> BaseDeDatos : 1.2. VALIDA DATOS DE USUARIO
BaseDeDatos-> User : 2. AUTORIZACION DE ACCESO 
@enduml
```
### Modelo de datos
Poner diseño de entidades, Jsons, tablas, diagramas entidad relación, etc..
```plantuml
@startuml

class Usuario {
    -idUsuario:integer
    +cedula:integer
    +nombre: string
    +apellido:  string
    +direccion: string
    +celular:integer

+{abstract} getNombre() String
+{abstract} setNombre() Void
+{abstract} getCedula() Integer
+{abstract} getApellido() String
+{abstract} setNombre() Void
+{abstract} getDireccion() String
+{abstract} setDireccion() Viod
+{abstract} getCelular() Integer
+{abstract} setCelular() Integer
}

class Cliente {
    -idCliente:integer
    +cedula:integer
    +nombre: string
    +apellido:  string
    +direccion: string
    +celular:integer
   
+{abstract} getNombre() String
+{abstract} setNombre() Void
+{abstract} getCedula() Integer
+{abstract} getApellido() String
+{abstract} setNombre() Void
+{abstract} getDireccion() String
+{abstract} setDireccion() Viod
+{abstract} getCelular() Integer
+{abstract} setCelular() Integer

}
class Vendedor {
    -idVendedor:integer
    +cedula:integer
    +nombre: string
    +apellido:  string
    +direccion: string
    +celular:integer
   
+{abstract} getNombre() String
+{abstract} setNombre() Void
+{abstract} getCedula() Integer
+{abstract} getApellido() String
+{abstract} setNombre() Void
+{abstract} getDireccion() String
+{abstract} setDireccion() Viod
+{abstract} getCelular() Integer
+{abstract} setCelular() Integer

}

class Administrador {
    -idAministrador:integer
    +cedula:integer
    +nombre: string
    +apellido:  string
    +direccion: string
    +celular:integer

+{abstract} getNombre() String
+{abstract} setNombre() Void
+{abstract} getCedula() Integer
+{abstract} getApellido() String
+{abstract} setNombre() Void
+{abstract} getDireccion() String
+{abstract} setDireccion() Viod
+{abstract} getCelular() Integer
+{abstract} setCelular() Integer
}



class Producto {
    -idProducto:integer
    +nombre: string
    +categoria:  string
    +decripcion: string
    +Precio:integer
    +fecha:date


+{abstract} getNombre() String
+{abstract} setNombre() Void
+{abstract} getCategoria() String
+{abstract} setCategoria() Void
+{abstract} getDescripcion() String
+{abstract} setDescripcion() Viod
+{abstract} getPrecio() Integer
+{abstract} setPrecio() Integer
+{abstract} getFecha() Date
}



(Cliente,Vendedor) ---> Usuario
Administrador -left---> Usuario
Vendedor -- " 0..*" Producto
@enduml
```

## Limitaciones
La plataforma web será desarrollada basada en ReactJS como framework front end, logrando así estabilidad, diseño responsive, seguridad al crear el build de la página en producción dando poco acceso al código fuente de la página para brindar seguridad. Teniendo en cuenta que la plataforma no tendrá módulo de registro de usuarios, se utilizará únicamente Javascript para traer los valores del API, por medio de Axios. NodeJS será necesario para el montaje del servidor de desarrollo de React. Las pruebas en las actualizaciones de los precios y componentes disponibles en el API de Amazon se harán por medio de POSTMAN, para después ser implementadas en el software. 

## Costo
Descripción/Análisis de costos
Ejemplo:
"Considerando N usuarios diarios, M llamadas a X servicio/baseDatos/etc"
* 1000 llamadas diarias a serverless functions. $XX.XX
* 1000 read/write units diarias a X Database on-demand. $XX.XX
Total: $xx.xx (al mes/dia/año)
